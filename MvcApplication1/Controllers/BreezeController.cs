﻿using System.Linq;
using System.Web.Http;
using Breeze.WebApi;
using MvcApplication1.Models;
using Newtonsoft.Json.Linq;

namespace MvcApplication1.Controllers
{
    [BreezeController]
    public class BreezeController : ApiController
    {
        private readonly EFContextProvider<CollaboratRDbContext> _contextProvider =
            new EFContextProvider<CollaboratRDbContext>();

        [HttpGet]
        public string Metadata()
        {
            return _contextProvider.Metadata();
        }

        [HttpPost]
        public SaveResult SaveChanges(JObject saveBundle)
        {
            return _contextProvider.SaveChanges(saveBundle);
        }

        [HttpGet]
        public IQueryable<Note> Notes()
        {
            return _contextProvider.Context.Notes;
        }
    }
}