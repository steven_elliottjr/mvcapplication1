﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;

namespace MvcApplication1.Controllers
{
    public class CollaboratorHub : Hub
    {
        public void Hello()
        {
            Clients.All.hello("Ping " + DateTime.Now.ToString());
        }
    }
}