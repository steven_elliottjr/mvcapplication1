﻿define(['services/logger'], function (logger) {
    var title = 'Home';
    var vm = {
        activate: activate,
        title: "All Notes",
        sendMessageAction: sendMessage,
        messageReceived: ko.observable()
    };

    function sendMessage() {
        logger.log("sending message to server");
        sig.server.hello();
    }

    var sig = $.connection.collaboratorHub;

    $.connection.hub.start().done(function () {
        vm.sendMessageAction;
    });

    // implement a function to receive the "Hello" ping from the server and update The ViewModel
    sig.client.hello = function (what) {
        vm.messageReceived(what);
        logger.log('Server says: ' + what, null, 'home', true);
    }
    //#region Internal Methods
    function activate() {
        logger.log(title + ' View Activated', null, title, true);
        return true;
    }
    //#endregion

    return vm;
});