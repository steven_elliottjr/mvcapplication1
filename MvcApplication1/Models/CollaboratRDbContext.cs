﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace MvcApplication1.Models
{
    public class CollaboratRDbContext : DbContext
    {
        public CollaboratRDbContext() : base (nameOrConnectionString:"CollaboratoRDbConnection")
        {
            
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer<CollaboratRDbContext>(
                new DropCreateDatabaseIfModelChanges<CollaboratRDbContext>());
        }

        public DbSet<Note> Notes { get; set; }
    }
}